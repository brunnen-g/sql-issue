# sql-issue

Sql issue from pumb manual qa interview

![task](sql-task.jpeg)

## Init database

Postgre db used!  

See db init scripts  

![init.sql](./sql/init.sql)  
![init.sql](sql/init.sql)  
![init.sql](/sql/init.sql)  
![init.sql][/sql/init.sql]

See test data scripts (order is important, if you run it!)  

![departments-init.sql](sql/departments-init.sql)  
![seo-init.sql](sql/seo-init.sql)  
![ba-init.sql](sql/ba-init.sql)  
![it-init.sql](sql/it-init.sql)  

