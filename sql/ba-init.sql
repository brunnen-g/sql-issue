CREATE OR REPLACE FUNCTION get_ba_department_id()
    RETURNS INTEGER
AS
$$
DECLARE seo_department_id INTEGER;
BEGIN
    SELECT id INTO seo_department_id FROM department WHERE department.name = 'ba';
    RETURN seo_department_id;
END;
$$
    LANGUAGE plpgsql STRICT;

CREATE OR REPLACE FUNCTION get_ba_chief_id()
    RETURNS INTEGER
AS
$$
DECLARE ba_chief_id INTEGER;
BEGIN
    SELECT id INTO ba_chief_id FROM department WHERE department.name = 'Butch Cassidy';
    RETURN ba_chief_id;
END;
$$
    LANGUAGE plpgsql STRICT;

INSERT INTO employee (department_id, name, salary)
VALUES (get_ba_department_id(), 'Butch Cassidy', 7000);
INSERT INTO employee (department_id, chief_id, name, salary)
VALUES (get_ba_department_id(), get_ba_chief_id(), 'Kid Curry', 9000);
INSERT INTO employee (department_id, chief_id, name, salary)
VALUES (get_ba_department_id(), get_ba_chief_id(), 'Bill (Tod) Carver', 3000);
INSERT INTO employee (department_id, chief_id, name, salary)
VALUES (get_ba_department_id(), get_ba_chief_id(), 'Ben Kilpatrick', 3000);
