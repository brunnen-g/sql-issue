CREATE OR REPLACE FUNCTION get_it_department_id()
    RETURNS INTEGER
AS
$$
DECLARE department_id INTEGER;
BEGIN
    SELECT id INTO department_id FROM department WHERE department.name = 'it';
    RETURN department_id;
END;
$$
    LANGUAGE plpgsql STRICT;

CREATE OR REPLACE FUNCTION get_it_chief_id()
    RETURNS INTEGER
AS
$$
DECLARE chief_id INTEGER;
BEGIN
    SELECT id INTO chief_id FROM department WHERE department.name = 'Jesse James';
    RETURN chief_id;
END;
$$
    LANGUAGE plpgsql STRICT;


INSERT INTO employee (department_id, name, salary)
VALUES (get_it_department_id(), 'Jesse James', 7000);
INSERT INTO employee (department_id, chief_id, name, salary)
VALUES (get_it_department_id(), get_it_chief_id(), 'Clarence Hite', 2000);
INSERT INTO employee (department_id, chief_id, name, salary)
VALUES (get_it_department_id(), get_it_chief_id(), 'Robert Woodson Hite', 3000);
INSERT INTO employee (department_id, chief_id, name, salary)
VALUES (get_it_department_id(), get_it_chief_id(), 'Alexander Franklin James', 3000);
