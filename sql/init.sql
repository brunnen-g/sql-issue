/* Create table department */
CREATE TABLE IF NOT EXISTS department (
    id SERIAL,
    name VARCHAR(100),
    CONSTRAINT department_pk PRIMARY KEY (id),
    CONSTRAINT department_name_unique UNIQUE (name)
);
/* Create table employee */
CREATE TABLE IF NOT EXISTS employee (
    id SERIAL,
    department_id INTEGER,
    chief_id INTEGER,
    name VARCHAR(100),
    salary NUMERIC,
    CONSTRAINT employee_pk PRIMARY KEY (id),
    CONSTRAINT department_id_fk FOREIGN KEY (department_id) REFERENCES department(id),
    CONSTRAINT chief_id_fk FOREIGN KEY (chief_id) REFERENCES employee(id)
);
