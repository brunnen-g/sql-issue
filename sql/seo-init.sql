
CREATE OR REPLACE FUNCTION get_seo_department_id()
    RETURNS INTEGER
AS
$$
DECLARE seo_department_id INTEGER;
BEGIN
    SELECT id INTO seo_department_id FROM department WHERE department.name = 'seo';
    RETURN seo_department_id;
END;
$$
    LANGUAGE plpgsql STRICT;

CREATE OR REPLACE FUNCTION get_seo_chief_id()
    RETURNS INTEGER
AS
$$
DECLARE seo_chief_id INTEGER;
BEGIN
    SELECT id INTO seo_chief_id FROM department WHERE department.name = 'Johnny Dillinger';
    RETURN seo_chief_id;
END;
$$
    LANGUAGE plpgsql STRICT;


INSERT INTO employee (department_id, name, salary)
VALUES (get_seo_department_id(), 'Johnny Dillinger', 6000);
INSERT INTO employee (department_id, chief_id, name, salary)
VALUES (get_seo_department_id(), get_seo_chief_id(), 'Baby Face Nelson', 2000);
INSERT INTO employee (department_id, chief_id, name, salary)
VALUES (get_seo_department_id(), get_seo_chief_id(), 'John "Red" Hamilton', 3000);
INSERT INTO employee (department_id, chief_id, name, salary)
VALUES (get_seo_department_id(), get_seo_chief_id(), 'Homer Van Meter', 3000);
       
